const express = require('express');
const cors = require('cors');
const {sequelize} = require('./models');
require('dotenv').config();
const inventoryController = require('./controllers/inventoryController');

const app=express();
const PORT = process.env.PORT || 8082;

//enabling cors
app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true,
  }));

//middleware for parsing json
app.use(express.json());
//inventory initialization
inventoryController.scheduleInventoryInitialization();

//routes
app.use('/restaurants',require('./routes/restaurantsRoute'));
app.use('/customers',require('./routes/customersRoute'));
app.use('/bookings',require('./routes/bookingsRoute'));
app.use('/slots',require('./routes/slotsRoute'));
app.use('/inventory',require('./routes/inventoryRoute'));
app.use('/auth',require('./routes/authRoute'));

// Error handling middleware(generic/global error handling)
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something went wrong!');
  });
  
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
//ensures that sequelize models are synchronized with the database  
sequelize.sync().then(() => {
  console.log('Database synchronized');
}).catch((error) => {
  console.error('Error synchronizing database:', error);
});
