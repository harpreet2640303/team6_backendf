// src/middleware/authMiddleware.js
require('dotenv').config();
const jwt = require('jsonwebtoken');
const secretKey = process.env.JWT_SECRET;

function generateToken(user) {
  const payload = { userId: user.id };
  const options = { expiresIn: '1h' };
  return jwt.sign(payload, secretKey, options);
}

module.exports = (req, res, next) => {
  let token = req.header('Authorization');
  console.log('Authorization header:',token);
  if (!token) {
    return res.status(401).json({ message: 'Unauthorized - Missing token' });
  }
  // Ensure token starts with "Bearer "
  if (!token.startsWith('Bearer ')) {
    console.log('Invalid token format');
    return res.status(401).json({ message: 'Unauthorized - Invalid token format' });
  }
  if (token) {
    token = token.split(' ')[1];
  }
  //console.log(token)
  try {
    const decoded = jwt.verify(token, secretKey);
    req.user = decoded.userId; 
    next();
  } catch (error) {
    if (error.name === 'TokenExpiredError') {
      return res.status(401).json({ message: 'Unauthorized - Expired token' });
    }
    if (error.name === 'JsonWebTokenError') {
      return res.status(401).json({ message: 'Unauthorized - Invalid token' });
    }
    //console.error(error);
    return res.status(500).json({ message: 'Unauthorized - Internal server error' });
  }
};