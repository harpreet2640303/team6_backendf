// src/routes/customerRoutes.js
const express = require('express');
const router = express.Router();
const customersController = require('../controllers/customersController');

// Get details of a specific customer route (requires authentication)
router.get('/:id', customersController.getCustomerDetails)
module.exports = router;
