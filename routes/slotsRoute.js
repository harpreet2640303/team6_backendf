// src/routes/slotRoutes.js
const express = require('express');
const router = express.Router();
const slotController = require('../controllers/slotsController');
const authMiddleware = require('../middleware/authMiddleware');

// router.get('/',authMiddleware,async(req,res)=>{
//   try{
//     await slotController.getSlot(req,res);
//   }catch(error){
//     console.error('Error in GET /slots',error);
//     res.status(500).json({error:'Internal Server Error'});
//   }
// });

router.get('/slots',async(req,res)=>{
  try{
    await slotController.getSlot(req,res);
  }catch(error){
    console.error('Error in GET /slots',error);
    res.status(500).json({error:'Internal Server Error'});
  }
});
// Get details of a specific slot route (requires authentication)
router.get('/:id', async (req, res) => {
  try {
    await slotController.getSlotDetails(req, res);
  } catch (error) {
    console.error('Error in GET /slots/:id:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Get all slots for a specific restaurant
router.get('/', async (req, res) => {
  try {
    await slotController.getSlots(req, res);
  } catch (error) {
    console.error('Error in GET /slots', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});
// // Get details of a specific slot route (requires authentication)
// router.get('/slots/:id', async (req, res) => {
//   try {
//     await slotController.getSlotDetails(req, res);
//   } catch (error) {
//     console.error('Error in GET /slots/:id:', error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

module.exports = router;
