// src/routes/authRoutes.js
const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');

// User Registration (POST)
router.post('/register', authController.registerUser);

// User Login with Rate Limiting (POST)
router.post('/login', authController.loginLimiter, authController.loginUser);

module.exports = router;
