// src/routes/bookingRoutes.js
const express = require('express');
const router = express.Router();
const bookingsController = require('../controllers/bookingsController');
const authMiddleware = require('../middleware/authMiddleware');

// Create a new booking route (requires authentication)
router.post('/', authMiddleware,bookingsController.createBooking);

// Get details of a specific booking route (requires authentication)
router.get('/:id', authMiddleware ,bookingsController.getBookingDetails);
module.exports = router;