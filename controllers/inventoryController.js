// const expressAsyncHandler = require('express-async-handler');
const Slot = require('../models').slots;
const Inventory = require('../models').inventories;
const cron = require('node-cron');

const inventoryController = {
  initializeInventory: async () => {
    try {
      const allSlots = await Slot.findAll();
      const inventoryData = allSlots.map((slot) => ({
        restaurant_id: slot.restaurant_id,
        slot_id: slot.id,
        quantity: slot.capacity,
      }));
      await Inventory.bulkCreate(inventoryData);
      console.log('Inventory initialized successfully.');
    } catch (error) {
      console.error('Error initializing inventory:', error.message);
    }
  },

  resetInventory: async () => {
    try {
      const allSlots = await Slot.findAll();
      for (const slot of allSlots) {
        await Inventory.update(
          { quantity: slot.capacity },
          { where: { slot_id: slot.id } }
        );
      }
      console.log('Inventory reset successfully.');
    } catch (error) {
      console.error('Error resetting inventory:', error.message);
    }
  },

  // Schedule inventory initialization and resetting
  scheduleInventoryInitialization: () => {
    // Schedule inventory initialization at application startup
    inventoryController.initializeInventory();

    // Schedule inventory resetting every 1 minute
    cron.schedule('0 0 * * *', async () => {
      try {
        await inventoryController.resetInventory();
      } catch (error) {
        console.error('Error occurred during inventory reset:', error.message);
      }
    });
  },
};

module.exports = inventoryController;
