// src/controllers/bookingController.js
const expressAsyncHandler = require('express-async-handler');
//const restaurantsController = require('../controllers/restaurantsController');
const Booking = require('../models').bookings;
const Slot = require('../models').slots;
//const Customer = require('../models').customers;
const Inventory = require('../models').inventories;

const bookingsController ={
  createBooking: expressAsyncHandler( async (req, res) => {
  try {
    const { slot_id, customer_id,customer_name, contact_number, booking_date, num_guests } = req.body;
    console.log(slot_id, customer_id,customer_name, contact_number, booking_date, num_guests);
    // Check if the slot is available

    const inventoryRecord = await Inventory.findOne({ where: { slot_id: slot_id } });
      if (!inventoryRecord || inventoryRecord.quantity < num_guests) {
        return res.status(400).json({ error: 'Not enough inventory available' });
      }

    // // Check if the customer exists
    // const customer = await Customer.findByPk(customer_id);
    // if (!customer) {
    //   return res.status(404).json({ error: 'Customer not found' });
    // }

    // Create a new booking
    const newBooking = await Booking.create({
      slot_id: slot_id,
      customer_id: customer_id,
      customer_name: customer_name,
      contact_number: contact_number,
      booking_date: booking_date,
      num_guests: num_guests,
    });

    await inventoryRecord.decrement('quantity', { by: num_guests });

    res.status(201).json({ message: 'Booking created successfully', booking: newBooking });
  } catch (error) {
    res.status(500).json({ error: 'Error creating booking', details: error.message });
  }
}),

// Get details of a specific booking
    getBookingDetails: expressAsyncHandler( async (req, res) => {
  try {
    const bookingId = req.params.id;
    const booking = await Booking.findByPk(bookingId);

    if (booking) {
      res.status(200).json({ booking });
    } else {
      res.status(404).json({ error: 'Booking not found' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Error retrieving booking details', details: error.message });
  }
}),
};

module.exports =bookingsController;