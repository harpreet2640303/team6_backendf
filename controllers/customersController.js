// src/controllers/customerController.js
const expressAsyncHandler = require('express-async-handler');
const { Model } = require('sequelize');
const Customer = require('../models').customers;


const customersController ={ 
  getCustomerDetails: expressAsyncHandler( async (req, res) => {
  try {
    const customerId = req.params.id;
    const customer = await Customer.findByPk(customerId);

    if (customer) {
      res.status(200).json({ customer });
    } else {
      res.status(404).json({ error: 'Customer not found' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Error retrieving customer details', details: error.message });
  }
}),
};

module.exports = customersController;