// src/controllers/slotController.js
const Sequelize = require('sequelize');
const slot = require('../models/').slots;
const inventory = require('../models').inventories;

exports.getSlot = async(req,res)=>{
  try{
    const slots = await slot.findAll();
    res.status(200).json({slots});
  }catch(error){
    res.status(500).json({ error: 'Error retrieving slots', details: error.message });
  }
}

// Get details of a specific slot
exports.getSlotDetails = async (req, res) => {
  try {
    const slotId = req.params.id;
    const slots = await slot.findByPk(slotId);

    if (slots) {
      res.status(200).json({ slots });
    } else {
      res.status(404).json({ error: 'Slot not found' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Error retrieving slot details', details: error.message });
  }
};

exports.getSlots = async (req, res) => {
  try {
    const restaurant_id = req.query.restaurant_id; 
    if (!restaurant_id) {
      return res.status(400).json({ error: 'Restaurant ID is required' });
    }
    
    // Fetch slots for the specified restaurant_id
    const slots = await slot.findAll({
      where: { restaurant_id: restaurant_id } 
    });

    // Fetch inventory entries for the specified restaurant_id
    const inventoryEntries = await inventory.findAll({
      where: { 
        restaurant_id: restaurant_id
        //quantity: { [Sequelize.Op.gt]: 0 } // Filter for entries with quantity greater than 0
      } 
    });

    // Filter slots based on available quantity in inventory
    const availableSlotIds = inventoryEntries.map(item => item.slot_id);
    const availableSlots = slots.filter(slot => availableSlotIds.includes(slot.id));

    res.status(200).json({ slots: availableSlots });
  } catch (error) {
    res.status(500).json({ error: 'Error retrieving slots', details: error.message });
  }
};
