// controllers/restaurantController.js
const expressAsyncHandler = require("express-async-handler");
const { Op } = require("sequelize");
const Restaurant = require("../models").restaurants;

const restaurantsController = {
  getRestaurants: expressAsyncHandler(async (req, res) => {
    try {
      const { page = 1, search = "" } = req.query;
      const ITEMS_PER_PAGE = 4;

      console.log(page);

      let whereCondition = {};

      if (search) {
        whereCondition = {
          [Op.or]: [
            { name: { [Op.like]: `%${search}%` } },
            { cuisine_type: { [Op.like]: `%${search}%` } },
            { location: { [Op.like]: `%${search}%` } },
          ],
        };
      }

      const allRestaurants = await Restaurant.findAll({
        where: whereCondition,
      });

      const totalCount = await Restaurant.count({ where: whereCondition });

      const pageCount = Math.ceil(totalCount / ITEMS_PER_PAGE);

      const startIdx = (page - 1) * ITEMS_PER_PAGE;
      const endIdx = startIdx + ITEMS_PER_PAGE;
      const paginatedRestaurants = allRestaurants.slice(startIdx, endIdx);

      console.log(paginatedRestaurants, paginatedRestaurants.length);

      res.status(200).json({ paginatedRestaurants, totalCount, pageCount });
    } catch (error) {
      res.status(500).json({
        error: "Error retrieving restaurants",
        details: error.message,
      });
    }
  }),

  getRestaurantDetails: expressAsyncHandler(async (req, res) => {
    try {
      const restaurantId = req.params.id;
      // console.log(Restaurant)
      const restaurants = await Restaurant.findByPk(restaurantId);

      if (restaurants) {
        res.status(200).json({ restaurants });
      } else {
        res.status(404).json({ error: "Restaurant not found" });
      }
    } catch (error) {
      res.status(500).json({
        error: "Error retrieving restaurant details",
        details: error.message,
      });
    }
  }),
};

module.exports = restaurantsController;
