 // src/controllers/authController.js
const validator = require('validator');
const { validationResult } = require('express-validator');
const rateLimit = require('express-rate-limit');
const customers = require('../models').customers;
//const bcrypt = require('bcrypt'); 
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

// Use environment variable for secret key
const secretKey = process.env.JWT_SECRET;

// Function to hash a password using crypto
const hashPassword = (password) => {
  const hash = crypto.createHash('sha256');
  hash.update(password);
  return hash.digest('hex');
};
// Function to compare passwords using crypto
const comparePasswords = (inputPassword, hashedPassword) => {
  const hashedInputPassword = hashPassword(inputPassword);
  return hashedInputPassword === hashedPassword;
};

const registerUserValidation = (req,res,next)=>{
  const {name,email,phone,password} = req.body;
  if (!validator.isLength(name, { min: 1 })) {
    return res.status(400).json({ error: 'Name is required' });
  }

  if (!validator.isEmail(email)) {
    return res.status(400).json({ error: 'Invalid email address' });
  }

  if (!validator.isMobilePhone(phone)) {
    return res.status(400).json({ error: 'Invalid phone number' });
  }

  if (!validator.isLength(password, { min: 6 })) {
    return res.status(400).json({ error: 'Password must be at least 6 characters' });
  }

  next();
};

// // User registration
const registerUser = async (req, res) => {
  try {
    // Validate input using express-validator
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body;

    // Check if the email is already registered
    const existingUser = await customers.findOne({ where: { email } });
    if (existingUser) {
      return res.status(400).json({ error: 'Email already registered' });
    }

    // Hash the password before storing it in the database
    //const hashedPassword = await bcrypt.hash(password, 10);
    const hashedPassword = hashPassword(password);

    // Create a new customer
    const newCustomer = await customers.create({
      name,
      email,
      password: hashedPassword,
    });

    res.status(201).json({ message: 'User registered successfully', customer: newCustomer });
  } catch (error) {
    res.status(500).json({ error: 'Error registering user', details: error.message });
  }
};

// User login with rate limiting
const loginLimiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minutes
  max: 10, // Max 10 login attempts within the window
  message: 'Too many login attempts. Please try again later.',
});

const loginUser = async (req, res) => {
  try { 6
    // Validate input using express-validator
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;
    console.log(req.body);
    // Check if the email is registered
    const user = await customers.findOne({ where: { email } });
    if (!user) {
      return res.status(401).json({ error: 'Invalid email or password' });
    }
    console.log(secretKey);
    console.log(req.body);
    console.log(user.password)
    console.log(hashPassword(password));
    // Compare the provided password with the hashed password in the database
    const passwordMatch = comparePasswords(password, user.password);
    if (!passwordMatch) {
      return res.status(401).json({ error: 'Invalid email or password' });
    }

    // Generate a JWT token for authentication
    const token = jwt.sign({ userId: user.id }, secretKey, { expiresIn: '1h' });
    console.log(token)
    res.status(200).json({ message: 'Login successful', token, customerId:user.id,customerName:user.name });
    // console.log(res.status)
  } catch (error) {
    res.status(500).json({ error: 'Error logging in', details: error.message });
  }
};

module.exports = { loginLimiter, loginUser, registerUser , registerUserValidation };
