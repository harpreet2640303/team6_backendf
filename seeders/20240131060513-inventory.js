'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('inventories', [
      // {
      //   id: '1',
      //   restaurant_id: 1, 
      //   slot_id: 1, // Replace with the actual slot_id
      //   quantity: 40,
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
      // {
      //   id: '2',
      //   restaurant_id: 1, // Replace with the actual restaurant_id
      //   slot_id: 2, // Replace with the actual slot_id
      //   quantity: 30,
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
      // {
      //   id: '3',
      //   restaurant_id: 2, // Replace with the actual restaurant_id
      //   slot_id: 1, // Replace with the actual slot_id
      //   quantity: 20,
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
      // {
      //   id: '4',
      //   restaurant_id: 2, // Replace with the actual restaurant_id
      //   slot_id: 2, // Replace with the actual slot_id
      //   quantity: 20,
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    // Remove the inserted data when rolling back the seed
    return queryInterface.bulkDelete('inventories', null, {});
  },
};
