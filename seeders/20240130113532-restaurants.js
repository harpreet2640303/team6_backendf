"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Insert data into the 'restaurants' table
    await queryInterface.bulkInsert(
      "restaurants",
      [
        {
          id: 1,
          name: "Italian Delight",
          image:
            "https://source.unsplash.com/photo-of-pub-set-in-room-during-daytime-poI7DelFiVA",
          location: "Hamirpur",
          cuisine_type: "Italian",
          description:
            "Authentic Italian dishes, from classic pasta to delectable desserts, delivering the taste of Italy.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          name: "Sushi Heaven",
          image:
            "https://source.unsplash.com/rectangular-beige-wooden-tables-and-chair-Ciqxn7FE4vE",
          location: "Solan",
          cuisine_type: "Japanese",
          description:
            "Modern elegance meets traditional Japanese flavors at Sushi Heaven, crafting exquisite sushi rolls.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          name: "Burger Bliss",
          image:
            "https://source.unsplash.com/brown-and-gray-concrete-store-nmpW_WwwVSc",
          location: "Gurgaon",
          cuisine_type: "American",
          description:
            "Burger Bliss redefines the American burger experience with juicy, flavorful burgers and mouthwatering toppings.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          name: "Spice Garden",
          image:
            "https://source.unsplash.com/bottles-in-display-cabinet-B3Jt_d5E2Lk",
          location: "Hamirpur",
          cuisine_type: "Indian",
          description:
            "Spice Garden takes you on a journey through Indian flavors, from savory curries to tandoori delights.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 5,
          name: "Mediterranean Flavors",
          image:
            "https://source.unsplash.com/a-restaurant-with-a-checkered-floor-and-wooden-walls-vA-bK6svd48",
          location: "Noida",
          cuisine_type: "Mediterranean",
          description:
            "Indulge in vibrant and fresh Mediterranean flavors, from Greek salads to succulent kebabs.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 6,
          name: "Tex-Mex Junction",
          image:
            "https://source.unsplash.com/dish-on-white-ceramic-plate-N_Y88TWmGwA",
          location: "Austin",
          cuisine_type: "Tex-Mex",
          description:
            "Authentic Tex-Mex with a blend of spices and vibrant colors, delivering a flavorful experience.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 7,
          name: "French Bistro",
          image:
            "https://source.unsplash.com/fempty-chairs-and-tables-inside-building-with-lights-turned-on-pbGmFQN7jpw",
          location: "Paris",
          cuisine_type: "French",
          description:
            "Charm of a French bistro with exquisite dishes, offering an immersive culinary experience.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 8,
          name: "Thai Spice Haven",
          image: "https://source.unsplash.com/empty-restaurant-YH7KYtYMET0",
          location: "Bangkok",
          cuisine_type: "Thai",
          description:
            "Rich and aromatic flavors of authentic Thai cuisine, indulging the senses in every dish.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 9,
          name: "Seafood Cove",
          image: "https://source.unsplash.com/chairs-near-pool-EeeKjwm2vDo",
          location: "Sydney",
          cuisine_type: "Seafood",
          description:
            "Fresh catch served with a spectacular ocean view, offering a delightful seafood experience.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 10,
          name: "Vegetarian Delight",
          image:
            "https://source.unsplash.com/brown-wooden-table-and-chairs-WIVZDXnLTX4",
          location: "San Francisco",
          cuisine_type: "Vegetarian",
          description:
            "A haven for plant-based enthusiasts, offering a diverse menu of vegetarian delights.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 11,
          name: "Steakhouse Royale",
          image:
            "https://source.unsplash.com/black-metal-framed-red-padded-chairs-and-table-_4qQsmE4VdA",
          location: "New York",
          cuisine_type: "Steakhouse",
          description:
            "Finest cuts of beef cooked to perfection at our upscale steakhouse, a royal dining experience.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 12,
          name: "Fusion Fusion",
          image:
            "https://source.unsplash.com/two-person-sitting-on-bar-stool-chair-in-front-of-bar-front-desk-8x_fFNrmeDk",
          location: "Tokyo",
          cuisine_type: "Fusion",
          description:
            "Innovative fusion of flavors from around the world, offering a unique and diverse experience.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 13,
          name: "Greek Oasis",
          image: "https://source.unsplash.com/people-at-restaurant-xBXF9pr6LQo",
          location: "Athens",
          cuisine_type: "Greek",
          description:
            "Transport yourself to the Mediterranean with our authentic Greek dishes, a true oasis.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 14,
          name: "Mexican Fiesta",
          image:
            "https://source.unsplash.com/black-kitchen-appliance-on-kitchen-island-with-pendant-lights-Kwdp-0pok-I",
          location: "Mexico City",
          cuisine_type: "Mexican",
          description:
            "Vibrant and bold flavors of Mexican cuisine, spicing up your taste buds with fiesta.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 15,
          name: "Café Serenity",
          image:
            "https://source.unsplash.com/people-in-front-of-cafe-desk-94taEmdowRw",
          location: "Barcelona",
          cuisine_type: "Café",
          description:
            "Cozy ambiance offering aromatic coffees and pastries, creating a serene and delightful café experience.",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    // Remove the inserted data if needed (optional)
    await queryInterface.bulkDelete("restaurants", null, {});
  },
};
