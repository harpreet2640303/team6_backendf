'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Insert booking data into the 'bookings' table
    return queryInterface.bulkInsert('bookings', [
      // {
      //   id:1,
      //   slot_id: 1, 
      //   customer_id: 1, 
      //   customer_name: 'Harpreet',
      //   contact_number: '1234567890', 
      //   booking_date:'',
      //   num_guests: 2,
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
      // {
      //   id:2,
      //   slot_id: 2, // Replace with the actual slot_id
      //   customer_id: 2, // Replace with the actual customer_id
      //   customer_name: 'Jane Doe', // Replace with the actual customer_name
      //   contact_number: '9876543210', // Replace with the actual contact_number
      //   booking_date: new Date(),
      //   num_guests: 3,
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
      // Add more booking items as needed
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    // Remove the inserted data when rolling back the seed
    return queryInterface.bulkDelete('bookings', null, {});
  },
};
