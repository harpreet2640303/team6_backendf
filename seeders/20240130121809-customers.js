// seeders/20220128150000-create-customers.js

'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('customers', [
      // {
      //   id:1,
      //   name: 'Harpreet',
      //   email: 'harpreet@gmail.com',
      //   password: 'happy', 
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
      // {
      //   id:2,
      //   name: 'Jane Smith',
      //   email: 'jane.smith@example.com',
      //   password: 'hashedpassword456', 
      //   createdAt: new Date(),
      //   updatedAt: new Date(),
      // },
      // Add more customer entries as needed
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    // Remove all customers
    await queryInterface.bulkDelete('customers', null, {});
  }
};
