

const restaurants = require('../models/restaurants'); 
module.exports = (sequelize,DataTypes)=>{
const slots = sequelize.define('slots', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  //allowNull:false means these fields cann't  bbe left empty when creating a new record

  restaurant_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: restaurants,
      key: 'id'
    }
  },
  start_time: {
    type: DataTypes.TIME,
    allowNull: false,
  },
  end_time: {
    type: DataTypes.TIME,
    allowNull: false,
  },
  capacity: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: DataTypes.NOW,
  },
  updatedAt: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: DataTypes.NOW,
  },
});
return slots
}