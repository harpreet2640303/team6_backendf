//models

const slots = require('../models/slots');
const customers = require('../models/customers');
module.exports = (sequelize,DataTypes)=>{
  const bookings = sequelize.define('bookings', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    slot_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: slots, 
        key: 'id' 
      },
    },
    customer_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: customers,
        key: 'id',
      },
    },
    customer_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact_number: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    booking_date: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    num_guests: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      //field: 'created_at',
      //when DataTypes.NOW is used as default value it automatically sets the field to current date and time by default when record is created 
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      //field: 'updated_at',
      defaultValue: DataTypes.NOW,
    },
  });
  return bookings;
  }
  