// src/models/customer.js
const crypto = require('crypto');
//const bcrypt = require('bcrypt');
module.exports = (sequelize,DataTypes)=>{
const customers = sequelize.define('customers', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    },
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
    //setter method called whenever a value is set for th e'password' field

    // set(value) {
    //   // Hashing the password before saving it to the database
    //   //salt value is random value added to password before it's hashed
    //   //salt should be uinque and stored in database along with hashed password

    //   // const hashedPassword = bcrypt.hashSync(value, bcrypt.genSaltSync(10));
    //   // this.setDataValue('password', hashedPassword);
    //   const hashedPassword = hashPassword(value);
    //   this.setDataValue('password', hashedPassword);
    // },
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    //field: 'createdAt',
    defaultValue: DataTypes.NOW,
  },
  updatedAt: {
    type: DataTypes.DATE,
    allowNull: false,
    //field: 'updatedAt',
    defaultValue: DataTypes.NOW,
  },
});
const hashPassword = (password) => {
  const hash = crypto.createHash('sha256');
  hash.update(password);
  return hash.digest('hex');
};
return customers
}